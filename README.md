## QA-Tests

This is a repository to manage scripts to automate the website "https://qa-test.avenuecode.com". This website was developed to the recruiters of AvenueCode analyse the candidate's performance on QA Test.

To Run this application, follow the steps:

1. Clone this repo to your machine.
2. Open the console (Terminal, Git bash, CMD, etc.) and navigate to the path that you clone.
3. In the repository root folder, run `mvn clean test`.

NOTE: One of the scenarios will crash because there is a bug found.
