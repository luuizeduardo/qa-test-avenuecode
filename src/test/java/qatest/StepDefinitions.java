package qatest;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import pages.MainPage;
import pages.SubTaskPage;
import pages.TasksPage;
import suport.Web;

public class StepDefinitions {

    private WebDriver driver;
    LoginPage login;
    MainPage mainPage;
    TasksPage tasksPage;
    SubTaskPage subTaskPage;

    @Before
    public void setUp() {
        driver = Web.createChrome();
    }

    @Given("I am on the login page")
    public void i_am_on_the_login_page() {
        login = new LoginPage(driver);
    }

    @When("I fill in Email field with {string}")
    public void i_fill_in_email_field_with(String string) {
        login.typeEmail(string);
    }

    @When("I fill in Password field with {string}")
    public void i_fill_in_Password_field_with(String string) {
        login.typePassword(string);
    }

    @When("I press Sign In button")
    public void i_press_sign_in_button() {
        mainPage =  login.submitLogin();
    }

    @Then("I should be on the home page")
    public void i_should_be_on_the_home_page() {
        System.out.println("Navigate to main page");
    }

    @Then("I should see {string}")
    public void i_should_see(String string) {
        mainPage.validateLoginMessage(string);
    }

    @Given("I am logged in the platform")
    public void i_am_logged_in_the_platform() {
        System.out.println("Start scenario tests");
    }

    @When("I click {string}")
    public void i_click(String string) {
        tasksPage = mainPage.acessMyTasks();
    }

    @Then("I should be on the tasks page")
    public void i_should_be_on_the_tasks_page() {
        System.out.println("Navigate to task page concluded");
    }

    @Then("I should see the message {string}")
    public void i_should_see_the_message(String string) {
        tasksPage.validateAcessMessage(string);
    }

    @When("I fill the task description with {string} and hit the ENTER button")
    public void i_fill_the_task_description_with(String string) {
        tasksPage.addTask(string);
    }

    @Then("I should see the task {string} appended on the list of created tasks")
    public void i_should_see_the_task_appended_on_the_list_of_created_tasks(String string) {
        tasksPage.validateCreatedTask(string);
    }

    @When("I have a task created")
    public void i_have_a_task_created() {
        tasksPage.validateTaskAppended();
    }

    @When("I click in the {string} button")
    public void i_click_in_the_button(String string) {
        subTaskPage = tasksPage.openSubTaskModal();
    }

    @When("I fill the subtask description with {string}")
    public void i_fill_the_subtask_description_with(String string) {
        subTaskPage.typeSubtaskDescription(string);
    }

    @When("I fill the due date with {string}")
    public void i_fill_the_due_date_with(String string) {
        subTaskPage.typeSubtaskDueDate(string);
    }

    @When("I click to add a subtask in add button")
    public void i_click_to_add_a_subtask_in_button() {
        subTaskPage.addSubtask();
        tasksPage = subTaskPage.closeModal();
    }

    @Then("I should see the subtask appended on the list of created subtasks")
    public void i_should_see_the_subtask_appended_on_the_list_of_created_subtasks() {
        Assert.assertTrue(tasksPage.getCountOfSubTasks().contains("(1) Manage Subtasks"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
