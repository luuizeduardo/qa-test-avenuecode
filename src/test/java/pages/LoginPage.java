package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver browser) {
        super(browser);
    }

    public LoginPage typeEmail(String email){
        browser.findElement(By.id("user_email")).sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password){
        browser.findElement(By.id("user_password")).sendKeys(password);
        return this;
    }

    public MainPage submitLogin(){
        browser.findElement(By.name("commit")).click();
        return new MainPage(browser);
    }
}
