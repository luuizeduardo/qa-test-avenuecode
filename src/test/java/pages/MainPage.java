package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage {

    public MainPage(WebDriver browser) {
        super(browser);
    }

    public MainPage validateLoginMessage(String message){
        browser.findElement(By.xpath("//div[contains(text(),'" + message + "')]"));
        return this;
    }

    public TasksPage acessMyTasks(){
        browser.findElement(By.xpath("//ul[@class=\"nav navbar-nav\"]//a[contains(text(), \"My Tasks\")]")).click();
        return new TasksPage(browser);
    }
}
