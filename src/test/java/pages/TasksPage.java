package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class TasksPage extends BasePage {

    public TasksPage(WebDriver browser) {
        super(browser);
    }

    public TasksPage validateAcessMessage(String message){
        browser.findElement(By.xpath("//div[@class=\"container\"]//h1[contains(text(),'" + message + "')]"));
        return this;
    }

    public TasksPage addTask(String description){
        browser.findElement(By.id("new_task")).sendKeys(description);
        browser.findElement(By.id("new_task")).sendKeys(Keys.ENTER);
        return this;
    }

    public TasksPage validateCreatedTask(String description){
        browser.findElement(By.xpath("//table/tbody/tr/td[2]/a[contains(text(),'" + description + "')]"));
        return this;
    }

    public TasksPage validateTaskAppended(){
        browser.findElement(By.xpath("//table/tbody/tr/td[4]//button"));
        return this;
    }

    public SubTaskPage openSubTaskModal(){
        browser.findElement(By.xpath("//table/tbody/tr/td[4]//button")).click();
        return new SubTaskPage(browser);
    }

    public String getCountOfSubTasks(){
        return browser.findElement(By.xpath("//table/tbody/tr/td[4]//button")).getText();
    }
}
