package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class SubTaskPage extends BasePage{

    public SubTaskPage(WebDriver browser) {
        super(browser);
    }

    public SubTaskPage typeSubtaskDescription(String description){
        browser.findElement(By.id("new_sub_task")).sendKeys(description);
        return this;
    }

    public SubTaskPage typeSubtaskDueDate(String dueDate){
        browser.findElement(By.id("dueDate")).sendKeys(Keys.chord(Keys.CONTROL, "a"));
        browser.findElement(By.id("dueDate")).sendKeys(dueDate);
        return this;
    }

    public SubTaskPage addSubtask(){
        browser.findElement(By.id("add-subtask")).click();
        return this;
    }

    public TasksPage closeModal(){
        browser.findElement(By.xpath("//div[contains(@class, \"modal-footer\")]/button")).click();
        return new TasksPage(browser);
    }

}
