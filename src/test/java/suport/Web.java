package suport;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriver createChrome(){
        // Search for the driver
        String driverDirectory = System.getProperty("user.dir")
                + "\\drivers\\chromedriver.exe";

        // Creates a instance of the driver
        System.setProperty("webdriver.chrome.driver", driverDirectory);

        // Set up the options for Chrome start maximized
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        WebDriver browser = new ChromeDriver(options);

        // Define the implicit wait
        browser.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        // Browse to the start page
        browser.get("https://qa-test.avenuecode.com/users/sign_in");

        return browser;
    }

}
