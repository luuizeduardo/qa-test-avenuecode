Feature: US 2 - Create a SubTask
    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

    Background:
        Given I am on the login page
        When I fill in Email field with "led.martins93@gmail.com"
        And I fill in Password field with "@venueCodeQA"
        And I press Sign In button
        Then I should be on the home page
        And I should see "Signed in successfully."

    Scenario: creating a new subtask
        Given I am logged in the platform
        When I click "My Tasks"
        And I have a task created
        And I click in the "Manage Subtasks" button
        And I fill the subtask description with "Subtask description"
        And I fill the due date with "10/22/2018"
        And I click to add a subtask in add button
        Then I should see the subtask appended on the list of created subtasks