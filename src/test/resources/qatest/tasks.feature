Feature: US 1 - Create Task
    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

    Background:
        Given I am on the login page
        When I fill in Email field with "led.martins93@gmail.com"
        And I fill in Password field with "@venueCodeQA"
        And I press Sign In button
        Then I should be on the home page
        And I should see "Signed in successfully."

    Scenario: acessing the tasks page
        Given I am logged in the platform
        When I click "My Tasks"
        Then I should be on the tasks page
        And I should see the message "Hey Luiz, this is your todo list for today:"

    Scenario: creating a new task
        Given I am logged in the platform
        When I click "My Tasks"
        And I fill the task description with "Task description" and hit the ENTER button
        Then I should see the task "Task description" appended on the list of created tasks